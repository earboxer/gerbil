# window.py
#
# Copyright 2021 Armen
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Handy', '1')
from gi.repository import Gtk, Gio, GdkPixbuf, GLib, Handy
from .gemini.request import GeminiRequest
from .filetypes.filetypes import FileTypes
from .filetypes.gemini import GeminiParser
from .spartan.request import SpartanRequest
from .bookmarks.bookmarks import Bookmarks
from .settings.settings import Settings
from .accounts.gemlogblue import GemlogBlue

import os
import re
import base64

@Gtk.Template(resource_path='/com/armen138/gerbil/window.ui')
class GerbilWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'GerbilWindow'
    Handy.init()
    body:Gtk.TextView = Gtk.Template.Child()
    go_button:Gtk.Button = Gtk.Template.Child()
    go_image:Gtk.Image = Gtk.Template.Child()
    refresh_image:Gtk.Image = Gtk.Template.Child()
    address:Gtk.Entry = Gtk.Template.Child()
    back_button:Gtk.Button = Gtk.Template.Child()
    up_button:Gtk.Button = Gtk.Template.Child()
    forward_button:Gtk.Button = Gtk.Template.Child()
    about_dialog:Gtk.AboutDialog = Gtk.Template.Child()
    create_bookmark_dialog:Gtk.Dialog = Gtk.Template.Child()
    target_uri:Gtk.Label = Gtk.Template.Child()
    target_label:Gtk.Entry = Gtk.Template.Child()
    hamburgermenu:Gtk.Popover = Gtk.Template.Child()
    input_dialog:Gtk.Dialog = Gtk.Template.Child()
    input_message:Gtk.Label = Gtk.Template.Child()
    input_text:Gtk.Entry = Gtk.Template.Child()
    save_file:Gtk.FileChooserDialog = Gtk.Template.Child()
    warning:Gtk.Revealer = Gtk.Template.Child()
    warning_label:Gtk.Label = Gtk.Template.Child()
    warning_informative:Gtk.Revealer = Gtk.Template.Child()
    warning_informative_label:Gtk.Label = Gtk.Template.Child()
    headerbar_switcher = Gtk.Template.Child()
    bottom_switcher = Gtk.Template.Child()
    app_stack:Gtk.Stack = Gtk.Template.Child()
    post_edit:Gtk.TextView = Gtk.Template.Child()
    post_title:Gtk.Entry = Gtk.Template.Child()
    edit_toolbar:Gtk.Toolbar = Gtk.Template.Child()
    gemlog_controls:Gtk.Revealer = Gtk.Template.Child()
    post_error:Gtk.Revealer = Gtk.Template.Child()
    accounts_dialog:Gtk.Dialog = Gtk.Template.Child()
    account_username:Gtk.Entry = Gtk.Template.Child()
    account_password:Gtk.Entry = Gtk.Template.Child()
    bookmarks_list:Gtk.ListBox = Gtk.Template.Child()
    post_page:Gtk.Box = Gtk.Template.Child()
    post_title_bar:Gtk.Box = Gtk.Template.Child()
    post_location:Gtk.Entry = Gtk.Template.Child()
    post_location_bar:Gtk.Box = Gtk.Template.Child()
    post_token_bar:Gtk.Box = Gtk.Template.Child()
    post_token:Gtk.Entry = Gtk.Template.Child()
    read_only_switch:Gtk.Switch = Gtk.Template.Child()
    titan_post:Gtk.Button = Gtk.Template.Child()

    # account_service_select:Gtk.ComboBox = Gtk.Template.Child()

    bookmarks = Bookmarks()
    settings = Settings()
    save_data = b""
    input_uri = ""
    uri = None
    title = ""
    history = []
    forwards = []
    gb = GemlogBlue()
    read_only = True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # accounts_list = self.account_service_select.get_model()
        # accounts_list = Gtk.ListStore(str)
        # accounts_list.append(["gemlog.blue"])
        # self.account_service_select.set_model(accounts_list)
        self.fileTypes = FileTypes(self)
        self.make_edit_toolbar()
        home_address = "about:gerbil"
        home_location = GLib.Uri.parse(home_address, GLib.UriFlags.PARSE_RELAXED)
        self.navigate(home_location)
        self.refresh_bookmarks()
        if self.settings.get_item("read_only"):
            self.read_only_switch.set_state(True)

    def set_read_only(self):
        read_only = True
        self.titan_post.hide()
        self.post_page.hide()
        self.settings.set_item("read_only", read_only)

    def set_read_write(self):
        read_only = False
        self.titan_post.show()
        self.post_page.show()
        self.settings.set_item("read_only", read_only)

    def make_edit_toolbar(self):
        # populate editor toolbar
        h1 = Gtk.ToolButton(label="H1")
        h2 = Gtk.ToolButton(label="H2")
        h3 = Gtk.ToolButton(label="H3")
        quote = Gtk.ToolButton(label=">")
        link = Gtk.ToolButton(label="🔗")
        pre = Gtk.ToolButton(label="{ }")
        bulletlist = Gtk.ToolButton()
        bulletlist.set_icon_name("view-list-bullet-symbolic")
        accounts = Gtk.ToolButton(label="accounts")
        # separator = Gtk.SeparatorToolItem()
        # separator.set_expand(True)
        # separator.show()
        h1.show()
        h2.show()
        h3.show()
        quote.show()
        link.show()
        pre.show()
        bulletlist.show()
        accounts.show()
        h1.connect("clicked", self.on_edit_insert_h1)
        h2.connect("clicked", self.on_edit_insert_h2)
        h3.connect("clicked", self.on_edit_insert_h3)
        link.connect("clicked", self.on_edit_insert_link)
        quote.connect("clicked", self.on_edit_insert_quote)
        pre.connect("clicked", self.on_edit_insert_preformatted)
        bulletlist.connect("clicked", self.on_edit_insert_bullet)
        accounts.connect("clicked", self.on_accounts_clicked)
        self.edit_toolbar.add(h1)
        self.edit_toolbar.add(h2)
        self.edit_toolbar.add(h3)
        self.edit_toolbar.add(quote)
        self.edit_toolbar.add(link)
        self.edit_toolbar.add(bulletlist)
        self.edit_toolbar.add(pre)
        # self.edit_toolbar.add(separator)
        self.edit_toolbar.add(accounts)

    def navigate_about_scheme(self, path):
        self.warning_informative.set_reveal_child(False)
        self.warning.set_reveal_child(False)
        if path == "bookmarks":
            self.on_bookmarks_button_clicked(None, None)
        elif path == "gerbil":
            # print(GLib.get_system_data_dirs())
            for data_dir in GLib.get_system_data_dirs():
                about_path = data_dir + "/gerbil/about/gerbil.gmi"
                # print("Try " + about_path);
                if os.path.exists(about_path):
                    with open(about_path, "r") as readme_file:
                        self.write_content(readme_file.read(), "text/gemini")
                    break
        else:
            self.write_content("# Error\r\nUnknown about page", "text/gemini")
        self.uri = GLib.Uri.parse(f"about:{path}", GLib.UriFlags.PARSE_RELAXED)
        self.address.set_text(self.uri.to_string())

    def navigate(self, uri:GLib.Uri, history=True, upload=None, allow_bad_certificate=False, trust_bad_certificate=False, token=None, clearForward=True):
        if self.uri and history:
            self.history.append(self.uri.to_string())
        if clearForward:
          # TODO: push onto history stack?
          self.forwards=[]
        self.back_button.set_sensitive(self.can_back())
        self.up_button.set_sensitive(self.can_up(uri))
        self.forward_button.set_sensitive(self.can_forward())
        if uri.get_scheme() == "about":
            self.navigate_about_scheme(uri.get_path())
            return
        if uri.get_scheme() == "gemini" or uri.get_scheme() == "titan":
            self.uri = uri
            gemini = GeminiRequest(allow_bad_certificate=allow_bad_certificate, trust_bad_certificate=trust_bad_certificate)
            if uri.get_scheme() == "titan":
                if upload != None:
                    response = gemini.open(uri.to_string(), payload=upload, token=token)
                else:
                    self.on_titan_post_clicked(self.titan_post, userdata=uri.to_string())
                    return
            else:
                response = gemini.open(uri.to_string())
            if gemini.certificate_warning:
                # print(gemini.certificate_warning)
                if gemini.allow_bad_certificate:
                    self.warning_informative_label.set_text(gemini.certificate_warning)
                    self.warning_informative.set_reveal_child(True)
                    self.warning.set_reveal_child(False)
                else:
                    self.warning_label.set_text(gemini.certificate_warning)
                    self.warning.set_reveal_child(True)
                    self.warning_informative.set_reveal_child(False)
            else:
                self.warning.set_reveal_child(False)
                self.warning_informative.set_reveal_child(False)
            self.uri = GLib.Uri.parse(response.uri, GLib.UriFlags.PARSE_RELAXED)
            if int(response.status / 10) == 2:
                self.write_content(response.body, response.mime)
            elif int(response.status) == 10:
                self.input_uri = response.uri
                self.input_message.set_text(response.meta)
                self.input_dialog.show()
            else:
                if response.status != 0: # error code 0 is a client-side error
                    self.write_content(f"# Error {response.status}\r\n{response.meta}", "text/gemini")
                else:
                    self.write_content(f"# Error\r\n{response.meta}", "text/gemini")
            self.address.set_text(self.uri.to_string())
            show_log_controls = self.gb.is_me(self.uri.to_string())
            if show_log_controls:
                self.gemlog_controls.set_reveal_child(True)
            else:
                self.gemlog_controls.set_reveal_child(False)
        elif uri.get_scheme() == "spartan":
            self.uri = uri
            spartan = SpartanRequest()
            response = spartan.open(uri.to_string(), upload)
            self.uri = GLib.Uri.parse(response.uri, GLib.UriFlags.PARSE_RELAXED)
            if int(response.status) == 2:
                self.write_content(response.body, response.mime)
            else:
                if response.status != 0: # error code 0 is a client-side error
                    self.write_content(f"# Error {response.status}\r\n{response.meta}", "text/gemini")
                else:
                    self.write_content(f"# Error\r\n{response.meta}", "text/gemini")
            self.address.set_text(self.uri.to_string())
        elif uri.get_scheme() == "data":
            # passing "silly test"
            if "base64" in uri.get_path():
                meta = uri.get_path().split("base64,", 1)[0]
                meta_properties = {
                    "charset": "utf-8"
                }
                for item in [ x.split("=") for x in meta.split(";") ]:
                    if len(item) > 1:
                        meta_properties[item[0].lower()] = item[1].lower()
                text = base64.b64decode(uri.get_path().split("base64,", 1)[1])
                # print(text.decode(meta_properties["charset"]))
                self.write_content(text.decode(meta_properties["charset"], "text/gemini"))
            else:
                self.write_content(uri.get_path().split(",", 1)[1], "text/gemini")
            self.uri = uri
            self.address.set_text(self.uri.to_string())
        elif uri.get_scheme() == "view-source":
            source_uri = uri.to_string().replace("view-source:", "")
            gemini = GeminiRequest()
            response = gemini.open(source_uri)
            buffer = Gtk.TextBuffer.new()
            buffer.set_text(response.body)
            self.body.set_buffer(buffer)
            self.uri = uri
            self.address.set_text(uri.to_string())
        else:
            print("This is not a gemini link")

    def write_content(self, text, mime = "text/gemini"):
        if "image" in mime:
            self.title = mime + "\n"
            tb = Gtk.TextBuffer()
            tb.set_text(self.title)
            pixdata = Gio.MemoryInputStream.new_from_bytes(GLib.Bytes.new(text))
            pixbuf = GdkPixbuf.Pixbuf.new_from_stream(pixdata)
            if pixbuf == None:
                tb.set_text(pixbuf.error)
            image = Gtk.Image.new_from_pixbuf(pixbuf)
            anchor = tb.create_child_anchor(tb.get_end_iter())
            self.body.set_buffer(tb)
            self.body.add_child_at_anchor(image, anchor)
            image.show()
        elif mime == "application/atom+xml" or (self.uri and self.uri.get_path().endswith("atom.xml")):
            self.fileTypes.feed(self.body, text)
        elif mime == "text/x-diff" or mime == "text/x-patch":
            self.fileTypes.diff(self.body, text)
        elif mime == "text/plain":
            self.fileTypes.text(self.body, text)
        elif mime == "text/gemini":
            self.fileTypes.gemini(self.body, text)
        else:
            filename = self.uri.get_path().split("/")[-1]
            self.title = "text"
            tb = Gtk.TextBuffer.new()
            tb.set_text(f"Unknown file type: {mime}\nFile name: {filename}\n")
            self.body.set_buffer(tb)
            self.save_data = text
            self.save_file.set_current_folder(os.path.join(GLib.get_home_dir(), "Downloads"))
            self.save_file.set_current_name(filename)
            self.save_file.show()
        self.go_button.set_image(self.refresh_image)

    def on_follow_link(self, button, uri):
        # print(uri)
        if not uri:
            return
        try:
            location = GLib.Uri.parse(uri, GLib.UriFlags.PARSE_RELAXED)
        except GLib.Error:
            location = GLib.Uri.resolve_relative(self.uri.to_string(), uri, GLib.UriFlags.PARSE_RELAXED)
            location = GLib.Uri.parse(location, GLib.UriFlags.PARSE_RELAXED)
        # print(location.to_string())
        self.navigate(location)

    def on_send_spartan_upload(self, entry, uri):
        try:
            location = GLib.Uri.parse(uri, GLib.UriFlags.PARSE_RELAXED)
        except GLib.Error:
            location = GLib.Uri.resolve_relative(self.uri.to_string(), uri, GLib.UriFlags.PARSE_RELAXED)
            location = GLib.Uri.parse(location, GLib.UriFlags.PARSE_RELAXED)
        # print(entry.get_text())
        self.navigate(location, upload=entry.get_text())

    def edit_line_type_at(self, prefix, line_id):
        known_line_types = [ "###", "##", "#", "*", ">", "=>", "```\n"]
        buffer = self.post_edit.get_buffer()
        line_iter = buffer.get_iter_at_line(line_id)
        line_end_iter = buffer.get_iter_at_line(line_id + 1)
        line = buffer.get_text(line_iter, line_end_iter, False)
        for known_line_type in known_line_types:
            if line.startswith(known_line_type):
                type_iter = buffer.get_iter_at_line(line_id)
                type_iter.forward_chars(len(known_line_type) + 1)
                buffer.delete(line_iter, type_iter)
                if known_line_type == prefix:
                    return
                break
        line_iter = buffer.get_iter_at_line(line_id)
        postfix = " "
        if '`' in prefix:
            postfix = ""
        buffer.insert(line_iter, prefix + postfix)

    def edit_line_type(self, prefix):
        buffer = self.post_edit.get_buffer()
        if buffer.get_has_selection():
            (start, end) = buffer.get_selection_bounds()
            for i in range(start.get_line(), end.get_line() + 1):
                self.edit_line_type_at(prefix, i)
        else:
            cursor = buffer.get_insert()
            line_id = buffer.get_iter_at_mark(cursor).get_line()
            self.edit_line_type_at(prefix, line_id)

    def on_edit_insert_h1(self, userdata=None):
        self.edit_line_type("#")

    def on_edit_insert_h2(self, userdata=None):
        self.edit_line_type("##")

    def on_edit_insert_h3(self, userdata=None):
        self.edit_line_type("###")

    def on_edit_insert_quote(self, userdata=None):
        self.edit_line_type(">")

    def on_edit_insert_link(self, userdata=None):
        self.edit_line_type("=>")

    def on_edit_insert_bullet(self, userdata=None):
        self.edit_line_type("*")

    def on_edit_insert_preformatted(self, userdata=None):
        buffer = self.post_edit.get_buffer()
        if buffer.get_has_selection():
            (start, end) = buffer.get_selection_bounds()
            offset = 0
            if not end.starts_line():
                offset += 1
            self.edit_line_type_at("```\n", end.get_line() + offset)
            (start, end) = buffer.get_selection_bounds()
            self.edit_line_type_at("```\n", start.get_line())
        else:
            cursor = buffer.get_insert()
            line_id = buffer.get_iter_at_mark(cursor).get_line()
            self.edit_line_type_at("```\n", line_id)
            self.edit_line_type_at("```\n", line_id + 2)

    def on_accounts_clicked(self, userdata=None):
        self.accounts_dialog.show()

    def on_navigate_bookmark(self, button, url):
        location = GLib.Uri.parse(url, GLib.UriFlags.PARSE_RELAXED)
        self.navigate(location)
        self.app_stack.set_visible_child_name("browse")


    def on_delete_bookmark(self, button, url):
        self.bookmarks.delete(url)
        self.refresh_bookmarks()
        # location = GLib.Uri.parse(url, GLib.UriFlags.PARSE_RELAXED)
        # self.navigate(location)
        # self.app_stack.set_visible_child_name("browse")

    def refresh_bookmarks(self):
        list_items = self.bookmarks.as_list_items()
        rows = self.bookmarks_list.get_children()
        for row in rows:
            self.bookmarks_list.remove(row)
        # self.bookmarks_list.remove_all()
        for item in list_items:
            (row, go_button, trash_button, url) = item
            go_button.connect("clicked", self.on_navigate_bookmark, url)
            trash_button.connect("clicked", self.on_delete_bookmark, url)
            self.bookmarks_list.add(row)

    def reset_post(self):
        self.post_edit.get_buffer().set_text("")
        self.post_location_bar.hide()
        self.post_location.set_text("")
        self.post_title_bar.show()
        self.post_title.set_sensitive(True)
        self.post_title.set_text("")
        self.post_token_bar.hide()

    @Gtk.Template.Callback()
    def on_navigate(self, button, userdata=None):
        address = self.address.get_text()
        scheme = GLib.Uri.peek_scheme(address)
        if not scheme:
            #if IS_URL_PATTERN.match(address):
            if "." in address and " " not in address:
                address = f"gemini://{address}"
            else:
                address = f"gemini://geminispace.info/search?{address}"
        location = GLib.Uri.parse(address, GLib.UriFlags.PARSE_RELAXED)
        # print("navigate to " + location.to_string())
        self.navigate(location)

    def can_back(self):
      return len(self.history) > 0
    def can_up(self, uri):
      if uri:
        return len(uri.to_string().replace('://','').rstrip('/').split('/')) > 1
      return False
    def can_forward(self):
      return len(self.forwards) > 0

    @Gtk.Template.Callback()
    def on_allow_once(self, button, userdata=None):
        address = self.address.get_text()
        location = GLib.Uri.parse(address, GLib.UriFlags.PARSE_RELAXED)
        self.navigate(location, allow_bad_certificate=True)

    @Gtk.Template.Callback()
    def on_trust_certificate(self, button, userdata=None):
        address = self.address.get_text()
        location = GLib.Uri.parse(address, GLib.UriFlags.PARSE_RELAXED)
        self.navigate(location, trust_bad_certificate=True)

    @Gtk.Template.Callback()
    def on_back(self, button, userdata=None):
        if len(self.history) > 0:
            self.forwards.append(self.uri.to_string())
            address = self.history.pop()
            location = GLib.Uri.parse(address, GLib.UriFlags.PARSE_RELAXED)
            self.navigate(location, history=False, clearForward=False)

    @Gtk.Template.Callback()
    def on_up(self, button, userdata=None):
      if self.uri.to_string():
        addr = self.uri.to_string()
        addr = '/'.join(addr.rstrip('/').split('/')[:-1]) +'/'
        location = GLib.Uri.parse(addr, GLib.UriFlags.PARSE_RELAXED)
        self.navigate(location)

    @Gtk.Template.Callback()
    def on_forward(self, button, userdata=None):
        if len(self.forwards) > 0:
            address = self.forwards.pop()
            location = GLib.Uri.parse(address, GLib.UriFlags.PARSE_RELAXED)
            self.navigate(location, clearForward=False)

    @Gtk.Template.Callback()
    def on_address_changed(self, button, userdata=None):
        self.go_button.set_image(self.go_image)

    @Gtk.Template.Callback()
    def on_about_button_clicked(self, button, userdata=None):
        self.hamburgermenu.hide()
        self.about_dialog.show()
        # self.about_dialog.maximize()

    @Gtk.Template.Callback()
    def on_close_about_clicked(self, button, userdata=None):
        self.about_dialog.hide()

    @Gtk.Template.Callback()
    def on_add_bookmark(self, button, userdata=None):
        self.hamburgermenu.hide()
        self.target_uri.set_text(self.uri.to_string())
        self.target_label.set_text(self.title)
        self.create_bookmark_dialog.show()

    @Gtk.Template.Callback()
    def on_cancel_bookmark_button_clicked(self, button, userdata=None):
        self.create_bookmark_dialog.hide()

    @Gtk.Template.Callback()
    def on_create_bookmark(self, button, userdata=None):
        self.create_bookmark_dialog.hide()
        target_label = self.target_label.get_text()
        self.bookmarks.store(self.uri.to_string(), target_label)
        self.refresh_bookmarks()

    def on_bookmarks_button_clicked(self, button, userdata=None):
        self.warning_informative.set_reveal_child(False)
        self.warning.set_reveal_child(False)
        self.hamburgermenu.hide()
        gmi = self.bookmarks.as_gmi()
        self.write_content(gmi)
        self.address.set_text("about:bookmarks")

    @Gtk.Template.Callback()
    def on_view_source_clicked(self, button, userdata=None):
        self.hamburgermenu.hide()
        if "view-source" not in self.uri.to_string():
            url = "view-source:" + self.uri.to_string()
            self.navigate(GLib.Uri.parse(url, GLib.UriFlags.PARSE_RELAXED))

    @Gtk.Template.Callback()
    def on_send_text_ok_button_clicked(self, button, userdata=None):
        text_to_send = GLib.Uri.escape_string(self.input_text.get_text(), None, True)
        send_uri = f"{self.input_uri}?{text_to_send}"
        self.input_dialog.hide()
        self.input_text.set_text("")
        self.navigate(GLib.Uri.parse(send_uri, GLib.UriFlags.PARSE_RELAXED))

    @Gtk.Template.Callback()
    def on_send_text_cancel_button_clicked(self, button, userdata=None):
        self.input_dialog.hide()
        self.input_text.set_text("")

    @Gtk.Template.Callback()
    def on_save_confirm_clicked(self, button, userdata=None):
        self.save_file.hide()
        # print(self.save_file.get_current_name)
        save_path = os.path.join(self.save_file.get_current_folder(), self.save_file.get_current_name())
        write_mode = "w"
        if isinstance(self.save_data, bytes):
            write_mode = "wb"
        with open(save_path, write_mode) as save_file:
            save_file.write(self.save_data)

    @Gtk.Template.Callback()
    def on_edit_clear_clicked(self, button, userdata=None):
        self.reset_post()
        # buffer = self.post_edit.get_buffer()
        # buffer.set_text("")
        # self.post_title.set_text("")
        # self.post_title.set_sensitive(True)
        # self.post_title_bar.show()
        # self.post_location_bar.hide()
        # self.post_token_bar.hide()

    @Gtk.Template.Callback()
    def on_save_cancel_clicked(self, button, userdata=None):
        self.save_file.hide()

    @Gtk.Template.Callback()
    def on_submit_post_clicked(self, button, userdata=None):
        title = self.post_title.get_text()
        location = self.post_location.get_text()
        buffer = self.post_edit.get_buffer()
        text = buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter(), False)
        token = self.post_token.get_text()
        if location.startswith("titan:"):
            print("post titan content")
            self.navigate(GLib.Uri.parse(location, GLib.UriFlags.PARSE_RELAXED), upload=text, token=token)
            self.app_stack.set_visible_child_name("browse")
            self.reset_post()
            return
        elif self.gb.is_me(title):
            new_post = self.gb.update(location, text)
        else:
            new_post = self.gb.post(title, text)
        if new_post:
            self.navigate(GLib.Uri.parse(new_post, GLib.UriFlags.PARSE_RELAXED))
            self.app_stack.set_visible_child_name("browse")
            self.reset_post()
        else:
            self.post_error.set_reveal_child(True)

    @Gtk.Template.Callback()
    def on_preview_post_clicked(self, button, userdata=None):
        buffer = self.post_edit.get_buffer()
        text = buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter(), False)
        # print(text)
        encoded = base64.b64encode(text.encode('utf-8')).decode('utf-8')
        preview = f"data:text/gemini;charset=utf-8;base64,{encoded}"
        # print(preview)
        self.navigate(GLib.Uri.parse(preview, GLib.UriFlags.PARSE_RELAXED), False)
        self.app_stack.set_visible_child_name("browse")

    @Gtk.Template.Callback()
    def on_squeezer_visible_child_notify(self, squeezer, userdata=None):
        child = squeezer.get_visible_child()
        self.bottom_switcher.set_reveal(child != self.headerbar_switcher)

    @Gtk.Template.Callback()
    def on_gemlog_delete_clicked(self, button, userdata=None):
        log = self.gb.delete(self.uri.to_string())
        if log:
            self.navigate(GLib.Uri.parse(log, GLib.UriFlags.PARSE_RELAXED), False)
            self.app_stack.set_visible_child_name("browse")

    @Gtk.Template.Callback()
    def on_gemlog_bar_response(self, button, userdata=None):
        # print("Close gemlog controls")
        self.gemlog_controls.set_reveal_child(False)

    @Gtk.Template.Callback()
    def on_gemlog_edit_clicked(self, button, userdata=None):
        gemini = GeminiRequest()
        response = gemini.open(self.uri.to_string())
        content = response.body
        self.post_edit.get_buffer().set_text(content)
        self.post_location.set_text(self.uri.to_string())
        self.post_location.set_sensitive(False)
        self.post_title_bar.hide()
        self.post_location_bar.show()
        self.post_token_bar.hide()
        self.app_stack.set_visible_child_name("post")

    @Gtk.Template.Callback()
    def on_accounts_cancel_clicked(self, button, userdata=None):
        self.accounts_dialog.hide()

    @Gtk.Template.Callback()
    def on_accounts_save_clicked(self, button, userdata=None):
        user = self.account_username.get_text()
        password = self.account_password.get_text()
        self.gb.save(user, password)
        self.accounts_dialog.hide()
        self.account_username.set_text("")
        self.account_password.set_text("")

    @Gtk.Template.Callback()
    def on_error_edit_account_clicked(self, button, userdata=None):
        self.on_accounts_clicked()
        self.post_error.set_reveal_child(False)

    @Gtk.Template.Callback()
    def on_bookmarks_list_row_activated(self, item, userdata=None):
        row = item.get_selected_row()
        # print(row)

    @Gtk.Template.Callback()
    def on_read_only_switch_state_set(self, switch, state, userdata=None):
        if state:
            self.set_read_only()
        else:
            self.set_read_write()

    @Gtk.Template.Callback()
    def on_titan_post_clicked(self, button, userdata=None):
        print("Oh yeah - titan posting!")
        url = userdata or self.uri.to_string()
        gemini = GeminiRequest()
        response = gemini.open(url)
        content = response.body
        if content:
            self.post_edit.get_buffer().set_text(content)

        self.post_token_bar.show()
        self.post_title_bar.hide()
        self.post_location_bar.show()
        # url = userdata or self.address.get_text()
        self.post_location.set_text(url.replace("gemini", "titan"))
        self.app_stack.set_visible_child_name("post")
