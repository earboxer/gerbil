import re
from gi.repository import Gio, GLib
from .response import GeminiResponse
from .identity import Identity
from ..store.store import Store

HEADER_PATTERN = re.compile("(\d\d)\s?(.*)")
MAX_REDIRECTS = 5

class GeminiRequest:
    def __init__(self, allow_bad_certificate=False, trust_bad_certificate=False):
        self.send = None
        self.receive = None
        self.redirects = 0
        self.certificate_warning = None
        self.identity = None
        self.certificate = None
        self.allow_bad_certificate = allow_bad_certificate
        self.trust_bad_certificate = trust_bad_certificate

    def on_accept_certificate(self, connection, peer_cert, errors):
        print("on accept cert", errors)
        if errors & Gio.TlsCertificateFlags.BAD_IDENTITY:
            print("bad identity")
            self.certificate_warning = "Certificate does not match server identity"
        if errors & Gio.TlsCertificateFlags.EXPIRED:
            self.certificate_warning = "Server certificate expired"
        if self.allow_bad_certificate:
            return True
        store = Store()
        if store.has_exception(peer_cert.props.certificate_pem):
            self.allow_bad_certificate = True
            return True
        if self.trust_bad_certificate:
            self.allow_bad_certificate = True
            store.add_exception(peer_cert.props.certificate_pem)
            return True

    def on_client_event(self, socket_client, event, connectable, connection):
        if event == Gio.SocketClientEvent.TLS_HANDSHAKING:
            print("TLS Connection ", connection)
            connection.connect("accept-certificate", self.on_accept_certificate)
            identity = Identity("Gerbil")
            connection.set_certificate(identity.certificate)
            print("before complete", connection.get_peer_certificate())
        if event == Gio.SocketClientEvent.TLS_HANDSHAKED:
            self.identity = connectable.to_string()
            self.certificate = connection.get_peer_certificate()
            peer_certificate = connection.get_peer_certificate()
            store = Store()
            is_trusted = store.is_trusted(self.identity, self.certificate.props.certificate_pem)
            if is_trusted == Store.UNKNOWN:
                store.trust(self.identity, self.certificate.props.certificate_pem)
            elif is_trusted == Store.UNTRUSTED:
                if self.trust_bad_certificate:
                    store.un_trust(self.identity)
                    store.trust(self.identity, self.certificate.props.certificate_pem)
                else:
                    print("This is not trusted!")
                    self.certificate_warning = "This certificate does not match the known certificate for this server."

    def open(self, url, payload = None, token = None):
        location = GLib.Uri.parse(url, GLib.UriFlags.PARSE_RELAXED)
        body = b""
        if location.get_scheme() == "titan" and payload != None:
            url = f"{url};size={len(payload.encode('utf-8'))};mime=text/plain"
            if token != None:
                url = f"{url};token={token}"
        elif location.get_scheme() != "gemini":
            return GeminiResponse(url, 0, "Gemini is the only supported protocol")

        client = Gio.SocketClient.new()
        client.set_timeout(10)
        client.set_tls(True)
        client.set_tls_validation_flags(Gio.TlsCertificateFlags.INSECURE | Gio.TlsCertificateFlags.BAD_IDENTITY | Gio.TlsCertificateFlags.EXPIRED)
        client.connect_after("event", self.on_client_event)
        try:
            port = location.get_port();
            if port == -1:
                port = 1965
            socket = client.connect_to_host(location.get_host(), port)
        except GLib.Error as e:
            return GeminiResponse(url, 0, e.message)
        if self.certificate_warning and not self.allow_bad_certificate:
            return GeminiResponse(url, 0, "Unacceptable TLS certificate")
        self.receive = socket.get_input_stream()
        self.send = socket.get_output_stream()
        if payload:
            self.send.write(f"{url}\r\n{payload}".encode('utf-8'))
        else:
            self.send.write(f"{url}\r\n".encode('utf-8'))
        data = GLib.Bytes.new()
        header = b""
        try:
            while b"\r\n" not in header:
                header += self.receive.read_bytes(1).get_data()
        except GLib.Error as e:
            return GeminiResponse(url, 0, e.message)
        match = HEADER_PATTERN.match(header.decode())
        meta_properties = {
            "charset": "utf-8", "lang": "en"
        }
        if match:
            status = int(match.group(1))
            meta = match.group(2)
            if int(status / 10) == 2:
                for item in [ x.strip().split("=") for x in meta.split(";") ]:
                    if len(item) > 1:
                        # property names are case-insensitive
                        meta_properties[item[0].lower()] = item[1]
                    elif "mime" not in meta_properties:
                        meta_properties["mime"] = item[0]
                print(meta_properties)
            else:
                meta_properties["mime"] = "text/plain"
            # the charset value is case-insensitive
            meta_properties["charset"] = meta_properties["charset"].lower()

        else:
            return GeminiResponse(url, 0, "Error decoding response header: " + header.decode())
        print("Status:", status)
        print("Meta:", meta)
        try:
            data = self.receive.read_bytes(4096)
            body = data.get_data()
        except GLib.Error as e:
            body = b"";
        if int(status / 10) == 2:
            raw = b""
            while data.get_size() > 0:
                try:
                    data = self.receive.read_bytes(4096)
                    raw = raw + data.get_data()
                except GLib.Error as e:
                    print(e)
                    break
            body = body + raw #.decode(meta_properties["charset"])
            if "gemini" in meta or "text" in meta:
                body = body.decode(meta_properties["charset"])

        if int(status / 10) == 3:
            self.redirects += 1
            if self.redirects > 5:
                return GeminiResponse(url, 0, "Too many redirects")
            redirect_url = meta
            if not meta.startswith("gemini:"):
                if "://" not in meta:
                    redirect_url = GLib.Uri.resolve_relative(url, meta, GLib.UriFlags.PARSE_RELAXED)
                else:
                    return GeminiResponse(url, 0, f"Cannot redirect here: {meta}")
            return self.open(redirect_url)
        return GeminiResponse(url, status, meta, body, meta_properties["mime"], meta_properties["lang"])

