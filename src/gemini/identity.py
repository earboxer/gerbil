from gi.repository import Gio, GLib
import os
import subprocess

class Identity:
    certificate = None
    def __init__(self, name):
        data_dir = GLib.get_user_data_dir()
        cert_path = os.path.join(data_dir, f"{name}.cert.pem")
        key_path = os.path.join(data_dir, f"{name}.key.pem")
        if not os.path.isfile(cert_path) or not os.path.isfile(key_path):
            generate_command =     f"openssl req -newkey rsa:2048 -x509 -nodes -days 9999 -subj \"/CN={name}\" -keyout {data_dir}/{name}.key.pem -out {data_dir}/{name}.cert.pem"
            subprocess.run([generate_command], shell=True, check=True)
        self.certificate = Gio.TlsCertificate.new_from_files(cert_path, key_path)

