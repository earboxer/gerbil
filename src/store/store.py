from gi.repository import Gio, GLib
import sqlite3
import os

SEED = ["""
CREATE TABLE IF NOT EXISTS certificates (
    identity text PRIMARY KEY,
    certificate bigtext NOT NULL
    );""",
"""
CREATE TABLE IF NOT EXISTS certificate_exceptions (
    identity text PRIMARY KEY
    );
"""
]

class Store:
    UNKNOWN = 0
    TRUSTED = 1
    UNTRUSTED = 2
    def __init__(self):
        data_dir = GLib.get_user_data_dir()
        self.db = sqlite3.connect(os.path.join(data_dir, 'gerbil.db'))
        cur = self.db.cursor()
        for seed in SEED:
            cur.execute(seed)
        self.db.commit()
    def __del__(self):
        self.db.close()
    def add_exception(self, identity):
        cur = self.db.cursor()
        cur.execute("INSERT INTO certificate_exceptions VALUES (?)", [identity])
        self.db.commit()
    def has_exception(self, identity):
        cur = self.db.cursor()
        cur.execute("SELECT * FROM certificate_exceptions WHERE identity = ?", [identity])
        item = cur.fetchone()
        return item != None
    def trust(self, identity, certificate):
        print("Trust", identity)
        cur = self.db.cursor()
        cur.execute("INSERT INTO certificates VALUES (?, ?)", (identity, certificate))
        self.db.commit()
    def un_trust(self, identity):
        cur = self.db.cursor()
        cur.execute("DELETE FROM certificates WHERE identity = ?", [ identity ])
        self.db.commit()
    def is_trusted(self, identity, certificate):
        print("Query", identity)
        cur = self.db.cursor()
        cur.execute("SELECT * FROM certificates WHERE identity = ?", [ identity ])
        item = cur.fetchone()
        if item == None:
            return Store.UNKNOWN
        (stored_identity, stored_certificate ) = item
        if stored_certificate == certificate:
            return Store.TRUSTED
        return Store.UNTRUSTED
