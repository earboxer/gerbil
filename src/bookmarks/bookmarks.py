from gi.repository import GLib, Gtk, Pango
import os
import json

class Bookmarks:
    items = []
    filename = os.path.join(GLib.get_user_data_dir(), "bookmarks.json")
    def __init__(self):
        if os.path.exists(self.filename):
            with open(self.filename, "r") as bookmarks_file:
                self.items = json.loads(bookmarks_file.read())

    def has_url(self, url):
        for item in self.items:
            if item['url'] == url:
                return True
        return False

    def delete(self, url):
        to_delete = None
        for item in self.items:
            if item["url"] == url:
                to_delete = item
                break
        self.items.remove(to_delete)
        with open(self.filename, "w") as bookmarks_file:
            bookmarks_file.write(json.dumps(self.items))

    def store(self, url, title):
        print(url, title)
        self.items.append({
            "url": url,
            "title": title
        })
        with open(self.filename, "w") as bookmarks_file:
            bookmarks_file.write(json.dumps(self.items))

    def as_gmi(self):
        gmi = [ "# Bookmarks" ]
        if len(self.items) == 0:
            gmi.append("You don't have any bookmarks  yet!")
        else:
            for item in self.items:
                gmi.append(f"=> {item['url']} {item['title']}")

        return "\n".join(gmi)

    def as_list_items(self):
        rows = []
        for item in self.items:
            row = Gtk.ListBoxRow()
            box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=4)
            row.add(box)
            trash_button = Gtk.Button.new_from_icon_name("user-trash-symbolic", Gtk.IconSize.BUTTON)
            go_button = Gtk.Button.new_from_icon_name("arrow1-right-symbolic", Gtk.IconSize.BUTTON)
            label = Gtk.Label(label=item["title"])
            label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR)
            box.pack_end(go_button, False, False, 12)
            box.pack_end(trash_button, False, False, 12)
            box.add(label)
            row.show_all()
            rows.append((row, go_button, trash_button, item["url"]))
        return rows
