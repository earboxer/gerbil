import re
from gi.repository import Gio, GLib
from .response import SpartanResponse
# from .identity import Identity

HEADER_PATTERN = re.compile("(\d)\s?(.*)")
MAX_REDIRECTS = 5

class SpartanRequest:
    def __init__(self):
        self.send = None
        self.receive = None
        self.redirects = 0

    def open(self, url, upload=None):
        location = GLib.Uri.parse(url, GLib.UriFlags.PARSE_RELAXED)
        body = b""
        if location.get_scheme() != "spartan":
            return SpartanResponse(url, 0, "Spartan is the only supported protocol")

        client = Gio.SocketClient.new()
        client.set_timeout(10)
        try:
            port = location.get_port()
            if port == -1:
                port = 300
            socket = client.connect_to_host(location.get_host(), port)
        except GLib.Error as e:
            return SpartanResponse(url, 0, e)
        self.receive = socket.get_input_stream()
        self.send = socket.get_output_stream()
        print(location.get_host(), location.get_path())
        upload_data = "0"
        if upload:
            upload_data = f"{len(upload)}\r\n{upload}"
        self.send.write(f"{location.get_host()} {location.get_path() or '/'} {upload_data}\r\n".encode('utf-8'))
        data = self.receive.read_bytes(4096)
        header = data.get_data().split(b"\r\n")[0]
        match = HEADER_PATTERN.match(header.decode())
        meta_properties = {
            "charset": "utf-8"
        }
        if match:
            status = int(match.group(1))
            meta = match.group(2)
            if int(status) == 2:
                for item in [ x.strip().split("=") for x in meta.split(";") ]:
                    if len(item) > 1:
                        # property names are case-insensitive
                        meta_properties[item[0].lower()] = item[1]
                    elif "mime" not in meta_properties:
                        meta_properties["mime"] = item[0]
                print(meta_properties)
            else:
                meta_properties["mime"] = "text/plain"
            # the charset value is case-insensitive
            meta_properties["charset"] = meta_properties["charset"].lower()

        else:
            return SpartanResponse(url, 0, "Error decoding response header")
        print("Status:", status)
        print("Meta:", meta)

        # header_data  = data.get_data().decode(meta_properties["charset"])
        if b"\r\n" in data.get_data():
            body = data.get_data().split(b"\r\n", 1)[1]
        else:
            body = b""
        if int(status) == 2:
            raw = b""
            while data.get_size() > 0:
                try:
                    data = self.receive.read_bytes(4096)
                    raw = raw + data.get_data()
                except GLib.Error as e:
                    print(e)
                    break
            body = body + raw #.decode(meta_properties["charset"])
            if "Spartan" in meta or "text" in meta:
                body = body.decode(meta_properties["charset"])

        if int(status) == 3:
            self.redirects += 1
            if self.redirects > 5:
                return SpartanResponse(url, 0, "Too many redirects")
            redirect_url = meta
            if not meta.startswith("spartan:"):
                if "://" not in meta:
                    if not meta.startswith("/"):
                        return SpartanResponse(url, 0, f"Relative redirect not allowed: {meta}")
                    redirect_url = GLib.Uri.resolve_relative(url, meta, GLib.UriFlags.PARSE_RELAXED)
                else:
                    return SpartanResponse(url, 0, f"Cross-protocol redirect not allowed: {meta}")
            redirect_location = GLib.Uri.parse(redirect_url, GLib.UriFlags.PARSE_RELAXED)
            if redirect_location.get_host() != location.get_host():
                return SpartanResponse(url, 0, f"Cross-domain redirect not allowed: {meta}")
            return self.open(redirect_url)
        return SpartanResponse(url, status, meta, body, meta_properties["mime"])

