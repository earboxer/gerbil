import requests
import re
import os
import json
import gi
gi.require_version('Secret', '1')
from gi.repository import Secret, GLib

GEMLOG_BLUE_POST_URL = "https://gemlog.blue/post.php"
GEMLOG_BLUE_DELETE_URL = "https://gemlog.blue/delete-post.php"
GEMLOG_BLUE_UPDATE_URL = "https://gemlog.blue/update-result.php"
GMI_TARGET_PATTERN = re.compile("gemini:\/\/.*\.gmi")
GEMLOG_USER_PATTERN = re.compile(".*gemlog.blue/users/(.*?)/.+")
GEMLOG_PARENT_PATTERN = re.compile(".*gemlog.blue/users/.*?/")

GEMLOG_BLUE_SCHEMA = Secret.Schema.new("blue.gemlog.credentials",
    Secret.SchemaFlags.NONE,
    {
        "service": Secret.SchemaAttributeType.STRING,
        "user": Secret.SchemaAttributeType.STRING
    }
)

class GemlogBlue:
    def __init__(self, user = None, password = None):
        self.identity = "gemlog.blue"
        self.user = user
        self.password = password
        if not user:
            user = self.get_known_user()
        if user:
            if not password:
                # pw =
                Secret.password_lookup(GEMLOG_BLUE_SCHEMA, { "service": "gemlog.blue", "user": user }, None, self.on_password_lookup, None)
                #if pw:
                #    self.password = pw
            elif password:
                Secret.password_store(GEMLOG_BLUE_SCHEMA, { "service": "gemlog.blue", "user": user }, Secret.COLLECTION_DEFAULT, "password", password, None, self.on_password_store, None)

    def on_password_lookup(self, source, result, userdata=None):
        pw = Secret.password_lookup_finish(result)
        if pw:
            self.password = pw

    def on_password_store(self, source, result, userdata=None):
        stored = Secret.password_store_finish(result)
        if stored:
            print("Success")
        else:
            print("Failed to store password")

    def get_known_user(self):
        filename = os.path.join(GLib.get_user_data_dir(), "gemlog.blue.json")
        if os.path.exists(filename):
            print("Known user file exists")
            with open(filename) as gemlog_blue_file:
                account = json.load(gemlog_blue_file)
                self.user = account["user"]
                print("Known user is ", account["user"])
                return account["user"]

    def is_me(self, url):
        if self.identity in url:
            match = GEMLOG_USER_PATTERN.match(url)
            if match:
                if self.user == match.group(1):
                    return True
        return False

    def update(self, url, content):
        response = requests.post(GEMLOG_BLUE_UPDATE_URL, data={
            "gemloguser": self.user,
            "pw": self.password,
            "url": url,
            "post": content
        })
        print(response.text)
        match = GMI_TARGET_PATTERN.search(response.text)
        print(match)
        print(match.group())
        if match:
            return match.group()
        return None

    def delete(self, url):
        response = requests.post(GEMLOG_BLUE_DELETE_URL, data={
            "gemloguser": self.user,
            "pw": self.password,
            "url": url
        })
        print(response.text)
        match = GEMLOG_PARENT_PATTERN.search(url)
        if match:
            return match.group()
        return None

    def post(self, title, content):
        response = requests.post(GEMLOG_BLUE_POST_URL, data={
            "gemloguser": self.user,
            "pw": self.password,
            "title": title,
            "post": content
        })
        print(response.text)
        if "Invalid username or password" in response.text:
            print("Bad password, oops")
        else:
            match = GMI_TARGET_PATTERN.search(response.text)
            if match:
                return match.group()
        return None

    def save(self, user, password):
        filename = os.path.join(GLib.get_user_data_dir(), "gemlog.blue.json")
        with open(filename, "w") as gemlog_blue_file:
            gemlog_blue_file.write(json.dumps({ "user": user }))
        Secret.password_store(GEMLOG_BLUE_SCHEMA, { "service": "gemlog.blue", "user": user }, Secret.COLLECTION_DEFAULT, "password", password, None, self.on_password_store, None)
        # Secret.password_store_sync(GEMLOG_BLUE_SCHEMA, { "service": "gemlog.blue", "user": user }, Secret.COLLECTION_DEFAULT, "password", password, None)
        self.user = user
        self.password = password
