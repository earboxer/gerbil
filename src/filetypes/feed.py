import feedparser

class Feed:
    def __init__(self, text):
        self.text = text
        self.data = feedparser.parse(text)
        print(self.data)
    def as_gemini(self):
        gemtext = [ "#" + self.data['feed']['title'], "\n" ]
        for post in self.data.entries:
            date = "%d-%02d-%02d" % (post.updated_parsed.tm_year,\
            post.updated_parsed.tm_mon, \
            post.updated_parsed.tm_mday)
            print("post date: " + date)
            print("post title: " + post.title)
            print("post link: " + post.link)
            gemtext.append(f"=> {post.link} {date} - {post.title}")
        return "\n".join(gemtext)
    def as_buffer(self):
        return None
