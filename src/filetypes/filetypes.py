from .gemini import GeminiParser
from .plaintext import PlainText
from .diff import Diff
from .feed import Feed
import os
import re
import base64
import gi
gi.require_version('Handy', '1')
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, GdkPixbuf, GLib, Handy

LINK_PATTERN = re.compile("\=\>\s?(.*?)\s(.*?)$")
NO_LABEL_LINK_PATTERN = re.compile("\=\>\s?(.*?)$")
INPUT_PATTERN = re.compile("=:\s?(.*?)\s(.*?)$")
NO_LABEL_INPUT_PATTERN = re.compile("=:\s?(.*?)$")

SUPPORTED = [
    "text/plain",
    "text/gemini",
    "text/x-diff",
    "text/x-patch"
]

class FileTypes:
    def __init__(self, window):
        self.window = window
    def is_supported(self, mime):
        return mime in SUPPORTED
    def feed(self, text_view, text):
        feed = Feed(text)
        gemtext = feed.as_gemini()
        self.gemini(text_view, gemtext)
    def diff(self, text_view, text):
        self.window.title = "diff"
        diff = Diff(text)
        text_view.set_buffer(diff.as_buffer())
    def text(self, text_view, text):
        # crudely try to detect diff served with plaintext mime
        if "diff --git" in text:
            return self.diff(text_view, text)
        self.window.title = "text"
        plaintext = PlainText(text)
        text_view.set_buffer(plaintext.as_buffer())
    def gemini(self, text_view, text):
        parser = GeminiParser(text, spartan = self.window.uri and self.window.uri.get_scheme() == "spartan")
        self.window.title = parser.get_title()
        tb = parser.as_buffer()
        text_view.set_buffer(tb)
        for anchor in parser.anchors:
            if anchor["type"] == "input":
                label = anchor["title"]
                link = anchor["title"]
                match = INPUT_PATTERN.match(link.strip())
                if match:
                    target = match.group(1)
                    label = match.group(2).strip()
                    if target == "":
                        target = match.group(2).strip()
                else:
                    match = NO_LABEL_INPUT_PATTERN.match(link)
                    if match:
                        target = match.group(1)
                        label = match.group(1).strip()
                    else:
                        print("Input not matching any input pattern", link)
                entry = Gtk.Box()
                label = Gtk.Label(label=label + " ")
                entry_input = Gtk.Entry()
                entry.add(label)
                entry.add(entry_input)
                entry_input.connect("activate", self.window.on_send_spartan_upload, target)
                entry.show_all()
                text_view.add_child_at_anchor(entry, anchor["anchor"])

            if anchor["type"] == "link":
                link = anchor["title"]
                label = link
                target = None
                match = LINK_PATTERN.match(link.strip())
                if match:
                    target = match.group(1)
                    label = match.group(2).strip()
                    if target == "":
                        target = match.group(2).strip()
                else:
                    match = NO_LABEL_LINK_PATTERN.match(link)
                    if match:
                        target = match.group(1)
                        label = match.group(1).strip()
                    else:
                        print("Link not matching any linking pattern")
                if target.startswith("http"):
                    link_button = Gtk.LinkButton(label=f"🕸 {label}", uri=target)
                elif target.startswith("gopher"):
                    link_button = Gtk.LinkButton(label=f"🐹 {label}", uri=target)
                elif target.startswith("mailto"):
                    link_button = Gtk.LinkButton(label=f"✉️ {label}", uri=target)
                elif self.window.bookmarks.has_url(target):
                    link_button = Gtk.LinkButton(label=f"🔖 {label}")
                    link_button.connect("clicked", self.window.on_follow_link, target)
                else:
                    link_button = Gtk.LinkButton(label=f"➤ {label}")
                    link_button.connect("clicked", self.window.on_follow_link, target)
                link_button.set_relief(Gtk.ReliefStyle.NONE)
                link_button.set_alignment(0.0, 0.5)
                link_button.set_tooltip_text(target)
                link_button.show_all()
                text_view.add_child_at_anchor(link_button, anchor["anchor"])
