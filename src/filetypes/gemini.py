import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Pango
import re
import html
from .diff import Diff

# using regex so we don't break ascii art
BULLET_LIST_PATTERN = re.compile("\*\s.+")

class GeminiParser:
    def __init__(self, text, spartan = False):
        self.spartan = spartan
        self.text = text
        self.anchors = []
        self.tags = {}
        self.last = None

    def get_title(self):
        line = self.text.split("\n")[0]
        if line.startswith(">") or line.startswith("#"):
            line = line[1:]
        return line[:40].strip()

    def as_buffer(self):
        buffer = Gtk.TextBuffer.new()
        self.tags["preformatted_tag"] = buffer.create_tag("preformatted", family="Monospace", wrap_mode=Gtk.WrapMode.NONE, indent=12, font="Liberation Mono Regular 10")
        # self.tags["quote"] =            buffer.create_tag("quote", indent=12, style=Pango.Style.ITALIC, paragraph_background="#b3b7d1", foreground="#23252d", pixels_above_lines=4, pixels_below_lines=4)
        self.tags["quote"] =            buffer.create_tag("quote", indent=12, style=Pango.Style.ITALIC, pixels_above_lines=4, pixels_below_lines=4)
        lines = self.text.split("\n")
        preformatted_block = False
        preformatted_lines = ""
        for line in lines:
            if line.startswith("```"):
                preformatted_block = not preformatted_block
                if not preformatted_block:
                    self.parse_line(preformatted_lines, buffer)
                    preformatted_lines = ""
            if preformatted_block:
                preformatted_lines += line + "\n"
            else:
                self.parse_line(line, buffer)
        return buffer

    def get_anchors(self, text):
        return {}

    def parse_line(self, line, buffer):
        if line.startswith("###"):
            self.last = "header"
            line = html.escape(line)
            line = f"<span weight='bold' size='large'>{line[3:].strip()}</span>\n"
            buffer.insert_markup(buffer.get_end_iter(), line, -1)
        elif line.startswith("##"):
            self.last = "header"
            line = html.escape(line)
            line = f"<span weight='bold' size='x-large'>{line[2:].strip()}</span>\n"
            buffer.insert_markup(buffer.get_end_iter(), line, -1)
        elif line.startswith("#"):
            self.last = "header"
            line = html.escape(line)
            line = f"<span weight='bold' size='xx-large'>{line[1:].strip()}</span>\n"
            buffer.insert_markup(buffer.get_end_iter(), line, -1)
        elif line.startswith(">"):
            line = line[1:].strip()
            buffer.insert_with_tags(buffer.get_end_iter(), line + "\n", self.tags["quote"])
            self.last = "quote"
        elif BULLET_LIST_PATTERN.match(line):
            line = html.escape(line)
            line = f"<span weight='normal' size='medium'>  • {line[1:].strip()}</span>\n"
            buffer.insert_markup(buffer.get_end_iter(), line, -1)
            self.last = "list"
        elif self.spartan and line.startswith("=:"):
            b = Gtk.TextChildAnchor()
            buffer.insert_child_anchor(buffer.get_end_iter(), b)
            self.anchors.append({
                "title": line.strip(),
                "anchor": b,
                "type": "input"
            })
            buffer.insert(buffer.get_end_iter(), "\n")
            self.last = "input"
        elif line.startswith("=>"):
            b = Gtk.TextChildAnchor()
            buffer.insert_child_anchor(buffer.get_end_iter(), b)
            self.anchors.append({
                "title": line.strip(),
                "anchor": b,
                "type": "link"
            })
            buffer.insert(buffer.get_end_iter(), "\n")
            self.last = "link"
        elif line.startswith("```"):
            caption_and_block = line.replace("```", "").strip().split("\n", 1)
            line = ""
            if len(caption_and_block) > 0 and caption_and_block[0] != "":
                caption = f"\n  <small>{caption_and_block[0]}</small>\n"
                buffer.insert_markup(buffer.get_end_iter(), caption, -1)
            # try inserting in-line diff
            if caption_and_block[-1].startswith("@@"):
                diff = Diff(caption_and_block[-1])
                buffer.insert_markup(buffer.get_end_iter(), diff.as_markup(), -1)
            else:
                buffer.insert_with_tags(buffer.get_end_iter(), caption_and_block[-1] + "\n", self.tags["preformatted_tag"])
        # if self.last == "header" or self.last == None:
        #     line = f"<span weight='bold' size='medium'>{line.strip()}</span>"
        #     buffer.insert_markup(buffer.get_end_iter(), line + "\n", -1)
        # else:
        elif line.strip() != "":
            self.last = "paragraph"
            buffer.insert(buffer.get_end_iter(), line + "\n")

        return
